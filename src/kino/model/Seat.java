package kino.model;

import javax.swing.*;

public class Seat {
    private int row;
    private int seat;
    private boolean reserved;
    private JButton button;

    public JButton getButton() {
        return button;
    }

    public void setButton(JButton button) {
        this.button = button;
    }

    public Seat(int row, int seat, JButton button) {
        this.row = row;
        this.seat = seat;
        this.reserved = false;
        this.button = button;
    }

    public int getRow() {
        return row;
    }

    public int getSeat() {
        return seat;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }
}