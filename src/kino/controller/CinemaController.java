package kino.controller;

import kino.model.Cinema;
import kino.model.Seat;
import kino.model.User;
import kino.view.CinemaSeatReservationApp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class CinemaController {
    private CinemaSeatReservationApp csra;
    private Cinema cinema;
    private User user;

    private Map<String, Map<Seat, String>> movieReservations = new HashMap<>();

    private Map<String, Map<Seat, String>> userReservations = userReservations = new HashMap<>();

    public CinemaController(CinemaSeatReservationApp csra, Cinema cinema) {
        this.csra = csra;
        this.cinema = cinema;
        setUpViewEvents();
    }

    private void setUpViewEvents() {
        this.csra.getLoginButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (user == null) {
                    String username = JOptionPane.showInputDialog("Enter username:");
                    if (username != null && !username.isEmpty()) {
                        user = new User(username);
                        csra.getUserLabel().setText("User: " + user.getUsername());
                        csra.getLoginButton().setEnabled(false);
                        csra.getLogoutButton().setEnabled(true);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "User already logged in.");
                }
            }
        });
        this.csra.getLogoutButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (user != null) {
                    user = null;
                    csra.getUserLabel().setText("User: Not logged in");
                    csra.getLoginButton().setEnabled(true);
                    csra.getLogoutButton().setEnabled(false);
                } else {
                    JOptionPane.showMessageDialog(null, "No user currently logged in.");
                }
            }
        });

        this.csra.getReserveButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StringBuilder reservationInfo = new StringBuilder();
                for (String movie : movieReservations.keySet()) {
                    Map<Seat, String> reservations = movieReservations.get(movie);
                    reservationInfo.append("Reserved Seats for ").append(movie).append(":\n");
                    if (reservations != null && !reservations.isEmpty()) {
                        for (Seat seat : reservations.keySet()) {
                            reservationInfo.append("Seat ").append(seat.getRow() + 1).append("-").append(seat.getSeat() + 1)
                                    .append(": ").append(reservations.get(seat)).append("\n");
                        }
                    } else {
                        reservationInfo.append("No reservations for this movie.\n");
                    }
                    reservationInfo.append("\n");
                }
                JOptionPane.showMessageDialog(null, reservationInfo.toString());
            }
        });

        // Action Listener für das Dropdown-Menü
        this.csra.getMovieComboBox().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetSeatButtons();
            }
        });
        for (Seat seat : this.csra.getSeats()) {
            seat.getButton().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (user != null && !seat.isReserved()) {
                        String customerName = JOptionPane.showInputDialog("Enter customer name:");
                        if (customerName != null && !customerName.isEmpty()) {
                            seat.setReserved(true);
                            seat.getButton().setBackground(Color.GRAY);
                            reserveSeatForCurrentUser(seat, customerName);
                        }
                    } else if (user == null) {
                        JOptionPane.showMessageDialog(null, "Please log in to make a reservation");
                    } else if (seat.isReserved()) {
                        JOptionPane.showMessageDialog(null, "This seat is already reserved");
                    }
                }
            });
        }
    }

    private void resetSeatButtons() {
        String selectedMovie = (String) csra.getMovieComboBox().getSelectedItem();
        Map<Seat, String> reservations = movieReservations.get(selectedMovie);

        Component[] components = csra.getContentPane().getComponents();
        for (Component component : components) {
            if (component instanceof JPanel && component != csra.getMoviePanel() && component != csra.getLoginPanel()) {
                JPanel panel = (JPanel) component;
                Component[] seatButtons = panel.getComponents();
                for (Component seatButton : seatButtons) {
                    if (seatButton instanceof JButton) {
                        JButton button = (JButton) seatButton;
                        Seat seat = getSeatFromButton(button);
                        if (seat != null && reservations != null && reservations.containsKey(seat)) {
                            // Sitz für den ausgewählten Film bereits reserviert
                            button.setEnabled(false);
                            button.setBackground(Color.GRAY);
                        } else {
                            // Sitz ist verfügbar
                            button.setEnabled(true);
                            button.setBackground(null);
                        }
                    }
                }
            }
        }
    }

    private void reserveSeatForCurrentUser(Seat seat, String customerName) {
        String selectedMovie = (String) csra.getMovieComboBox().getSelectedItem();
        Map<Seat, String> reservations = movieReservations.get(selectedMovie);
        if (reservations == null) {
            reservations = new HashMap<>();
            movieReservations.put(selectedMovie, reservations);
        }
        reservations.put(seat, customerName);
    }

    private Seat getSeatFromButton(JButton button) {
        for (Seat seat : csra.getSeats()) {
            if (seat.getButton() == button) {
                return seat;
            }
        }
        return null;
    }
}
