package kino.view;

import kino.model.Cinema;
import kino.model.Seat;
import kino.model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CinemaSeatReservationApp extends JFrame {
    private Cinema cinema;
    private static CinemaSeatReservationApp instance;
    private JComboBox<String> movieComboBox;

    public JComboBox<String> getMovieComboBox() {
        return movieComboBox;
    }

    public void setMovieComboBox(JComboBox<String> movieComboBox) {
        this.movieComboBox = movieComboBox;
    }


    private JPanel moviePanel;
    private JPanel loginPanel;
    private JButton loginButton;
    private JButton logoutButton;
    private JButton reserveButton;
    private JLabel userLabel;
    private JLabel movieLabel;

    public JPanel getLoginPanel() {
        return loginPanel;
    }
    public void setLoginPanel(JPanel loginPanel) {
        this.loginPanel = loginPanel;
    }
    public JPanel getMoviePanel() {
        return moviePanel;
    }
    public void setMoviePanel(JPanel moviePanel) {
        this.moviePanel = moviePanel;
    }

    public JLabel getMovieLabel() {
        return movieLabel;
    }

    public void setMovieLabel(JLabel movieLabel) {
        this.movieLabel = movieLabel;
    }

    public JButton getReserveButton() {
        return reserveButton;
    }

    public void setReserveButton(JButton reserveButton) {
        this.reserveButton = reserveButton;
    }

    public JButton getLogoutButton() {
        return logoutButton;
    }

    public void setLogoutButton(JButton logoutButton) {
        this.logoutButton = logoutButton;
    }

    public JLabel getUserLabel() {
        return userLabel;
    }

    public void setUserLabel(JLabel userLabel) {
        this.userLabel = userLabel;
    }

    public JButton getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(JButton loginButton) {
        this.loginButton = loginButton;
    }


    private ArrayList<Seat> seats = new ArrayList<Seat>();

    public ArrayList<Seat> getSeats() {
        return seats;
    }

    public void setSeats(ArrayList<Seat> seats) {
        this.seats = seats;
    }

    public CinemaSeatReservationApp() {
        setTitle("Cinema Seat Reservation");
        setSize(800, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        cinema = new Cinema(6, 6);


        // Filme Dropdown Auswahlliste erstellen
        moviePanel = new JPanel(new FlowLayout());
        movieLabel = new JLabel("Select Movie:");
        movieComboBox = new JComboBox<>();
        movieComboBox.addItem("König der Löwen");
        movieComboBox.addItem("Titanic");
        movieComboBox.addItem("Dirty Dancing");
        movieComboBox.addItem("Harry Potter");
        moviePanel.add(movieLabel);
        moviePanel.add(movieComboBox);

        add(moviePanel, BorderLayout.NORTH);

        // sitze erstellen
        JPanel seatPanel = new JPanel(new GridLayout(cinema.getRows(), cinema.getSeatsPerRow()));

        for (int row = 0; row < cinema.getRows(); row++) {
            for (int col = 0; col < cinema.getSeatsPerRow(); col++) {
                JButton seatButton = new JButton("Seat " + (row + 1) + "-" + (col + 1));
                Seat seat = new Seat(row, col, seatButton);
                seats.add(seat);
                seatPanel.add(seatButton);
            }
        }
        add(seatPanel, BorderLayout.CENTER);

        // Reserve Button erstellen
        reserveButton = new JButton("Reserve");
        add(reserveButton, BorderLayout.SOUTH);

        // login Panel erstellen
        loginPanel = new JPanel(new FlowLayout());
        loginButton = new JButton("Login");
        logoutButton = new JButton("Logout");
        userLabel = new JLabel("User: Not logged in");
        loginPanel.add(loginButton);
        loginPanel.add(logoutButton);
        loginPanel.add(userLabel);
        add(loginPanel, BorderLayout.WEST);


        setVisible(true);
    }

}