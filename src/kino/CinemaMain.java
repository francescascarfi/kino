package kino;

import kino.controller.CinemaController;
import kino.model.Cinema;
import kino.view.CinemaSeatReservationApp;

import javax.swing.*;

public class CinemaMain {
    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
           CinemaSeatReservationApp csra = new CinemaSeatReservationApp();
           Cinema c = new Cinema(6, 6);
           CinemaController cc = new CinemaController(csra, c);
        });
    }
}
